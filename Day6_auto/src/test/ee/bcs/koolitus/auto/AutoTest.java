package test.ee.bcs.koolitus.auto;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import ee.bcs.koolitus.auto.Auto;
import ee.bcs.koolitus.auto.Mootor;

public class AutoTest {
	Auto testAuto;

	@Before
	public void setUp() {
		testAuto = new Auto();
	}

	@After
	public void tearDown() {

	}

	@Test
	public void testArvutaKindlustusWithPeriodPositive() {
		final int PERIOOD_1 = 1;
		int kindlustuseMakse = testAuto.arvutaKindlustus(PERIOOD_1);
		Assert.assertEquals(10 * PERIOOD_1, kindlustuseMakse);

		final int PERIOOD_100 = 100;
		kindlustuseMakse = testAuto.arvutaKindlustus(PERIOOD_100);
		Assert.assertEquals(10 * PERIOOD_100, kindlustuseMakse);
	}

	@Test
	public void testArvutaKindlustusWithPeriodNegative() {
		final int PERIOOD_1 = -1;
		int kindlustuseMakse = testAuto.arvutaKindlustus(PERIOOD_1);
		Assert.assertEquals(0, kindlustuseMakse);
	}

	@Test
	public void testArvutaKindlustusWithPeriodZero() {
		final int PERIOOD_0 = 0;
		int kindlustuseMakse = testAuto.arvutaKindlustus(PERIOOD_0);
		Assert.assertEquals(0, kindlustuseMakse);
	}

	@Test
	public void testArvutaKindlustusWithNameMatiAndPeriodPositive() {
		final int PERIOOD_10 = 10;
		final String MATI = "Mati";
		int kindlustuseMakse = testAuto.arvutaKindlustus(MATI, PERIOOD_10);
		Assert.assertEquals(155, kindlustuseMakse);
	}

	@Test
	public void testArvutaKindlustusWithNameMatiAndPeriodNegative() {
		final int PERIOOD_1 = -1;
		final String MATI = "Mati";
		int kindlustuseMakse = testAuto.arvutaKindlustus(MATI, PERIOOD_1);
		Assert.assertEquals(0, kindlustuseMakse);
	}

	@Test
	public void testArvutaKindlustusWithNameEmptyAndPeriodPositive() {
		final int PERIOOD_1 = 1;
		final String EMPTY = "";
		int kindlustuseMakse = testAuto.arvutaKindlustus(EMPTY, PERIOOD_1);
		Assert.assertEquals(0, kindlustuseMakse);
	}

	@Test
	public void testIfAutoIsCreatedProperly() {
		Mootor mootor = new Mootor();
		final int USTE_ARV = 5;
		Auto testAuto = new Auto(USTE_ARV, mootor);
		Assert.assertEquals(USTE_ARV, testAuto.getUsteArv());
		Assert.assertEquals(mootor, testAuto.getMootor());
	}
}
