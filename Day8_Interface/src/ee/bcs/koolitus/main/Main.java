package ee.bcs.koolitus.main;

import ee.bcs.koolitus.interfaces.Kolmnurk;
import ee.bcs.koolitus.interfaces.Kujund;
import ee.bcs.koolitus.interfaces.Ruut;

public class Main {

	public static void main(String[] args) {
		Kujund ruut = new Ruut()
				.setKyljePikkus(5);
		System.out.println(ruut
				.arvutaPindala());
		
		Kujund kolmnurk = new Kolmnurk().setAlusePikkus(3.2456).setKorgusePikkus(2);
		
		System.out.println(kolmnurk.arvutaPindala());
	}

}
