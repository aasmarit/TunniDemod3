package ee.bcs.koolitus.abstractclass;

public class Opetaja extends Tootaja {
	@Override
	public void soomine() {
		System.out.println(getNimi() + " - ei ole aega süüa, tüübid lollitavad");
	}
}
